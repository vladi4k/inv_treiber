#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/delay.h>



static struct task_struct *thread_id1;
static struct task_struct *thread_id2;
static wait_queue_head_t wq;
static DECLARE_COMPLETION( on_exit );


DEFINE_MUTEX(mutex);

//mutex_init(&mutex);


static int thread_code( void *data)
{
	unsigned long timeout;
	int i;
	
	allow_signal( SIGTERM );
	for( i=0; i<5 && (kthread_should_stop() == 0); i++)
	{
		while( mutex_trylock( &mutex ) ==0) {
			printk("%s can't  sleep ... \n", (char *) data);
			//timeout=q230;
	                //timeout=wait_event_interruptible_timeout(wq, (timeout==0), timeout);
			mdelay(250);
		}

		timeout=HZ;
		timeout=wait_event_interruptible_timeout(wq, (timeout==0), timeout);
		mutex_unlock( &mutex );
		printk("%s thread function woke up ... %ld\n", (char *) data, timeout);
		mdelay(350);
		
		if(timeout== -ERESTARTSYS)
		{
			printk("%s go Signal break\n", (char *) data);
			break;
		}
	}
	complete_and_exit( &on_exit, 0);
}

static int __init kthread_init(void)
{
	init_waitqueue_head(&wq);
	thread_id1=kthread_create(thread_code, (void *) "mykthread1", "mykthread1" );
	thread_id2=kthread_create(thread_code, (void *) "mykthread2", "mykthread2" );
	if(thread_id1 ==0 || thread_id2 ==0)
		return -EIO;
	wake_up_process(thread_id1);
	wake_up_process(thread_id2);
	return 0;
}

static void __exit kthread_exit(void)
{
	kill_pid(task_pid(thread_id1), SIGTERM, 1);
	kill_pid(task_pid(thread_id2), SIGTERM, 1);
	wait_for_completion( &on_exit );
	wait_for_completion( &on_exit );
	printk("\n-----------------------------------------------------------\n\n");
}

module_init( kthread_init );
module_exit( kthread_exit );

/* Metainformation */
MODULE_LICENSE("GPL");
