#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x8495e121, "module_layout" },
	{ 0xe49685e5, "cdev_del" },
	{ 0x480b818b, "class_destroy" },
	{ 0x4e6693ef, "device_destroy" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0x619e82bd, "kobject_put" },
	{ 0xfafe0694, "device_create" },
	{ 0xd7a6b38a, "__class_create" },
	{ 0x149a0346, "cdev_add" },
	{ 0xe608c13c, "cdev_alloc" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0x27e1a049, "printk" },
	{ 0xb4390f9a, "mcount" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "860B9C6E869C1BE134299B6");
