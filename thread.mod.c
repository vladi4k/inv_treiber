#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x8495e121, "module_layout" },
	{ 0x6d0aba34, "wait_for_completion" },
	{ 0x6f20fdce, "kill_pid" },
	{ 0x8124a530, "wake_up_process" },
	{ 0x8edd3ac, "kthread_create_on_node" },
	{ 0x6395be94, "__init_waitqueue_head" },
	{ 0xa7f6f440, "mutex_unlock" },
	{ 0xfa66f77c, "finish_wait" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x5c8b5ce8, "prepare_to_wait" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xad816a88, "mutex_trylock" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x27e1a049, "printk" },
	{ 0x5aeb145f, "complete_and_exit" },
	{ 0xd2965f6f, "kthread_should_stop" },
	{ 0xd79b5a02, "allow_signal" },
	{ 0xdf9dc79c, "current_task" },
	{ 0xb4390f9a, "mcount" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "597C2F847ABDAE7B7A06C2C");
