#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>

static dev_t hello_dev_number;
static struct cdev *driver_object;
static struct class *hello_class;
static struct device *hello_dev;

static struct file_operations fops = {
	.owner= THIS_MODULE,
};

static int __init mod_init( void )
{
	printk("mod_init\n");
	if( alloc_chrdev_region(&hello_dev_number,0,1,"Hello")<0 )
		return -EIO;
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if( driver_object==NULL )
		goto free_device_number;
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if( cdev_add(driver_object,hello_dev_number,1) )
		goto free_cdev;
	/* Eintrag im Sysfs, damit Udev den Geraetedateieintrag erzeugt. */
	hello_class = class_create( THIS_MODULE, "Hello" );
	if( IS_ERR( hello_class ) ) {
		pr_err( "hello: no udev support\n");
		goto free_cdev;
	}
	hello_dev = device_create( hello_class, NULL, hello_dev_number,
			NULL, "%s", "hello" );
	return 0;
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region( hello_dev_number, 1 );
	return -EIO;
}

static void __exit mod_exit( void )
{
	printk("mod_exit\n");
	/* Loeschen des Sysfs-Eintrags und damit der Geraetedatei */
	device_destroy( hello_class, hello_dev_number );
	class_destroy( hello_class );
	/* Abmelden des Treibers */
	cdev_del( driver_object );
	unregister_chrdev_region( hello_dev_number, 1 );
	return;
}

module_init( mod_init );
module_exit( mod_exit );

/* Metainformation */
MODULE_LICENSE("GPL");
